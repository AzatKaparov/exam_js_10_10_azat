import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchNews} from "../../store/actions/newsActions";
import {Button} from "react-bootstrap";
import ArticleItem from "../../components/ArticleItem/ArticleItem";
import {NavLink} from "react-router-dom";
import Preloader from "../../components/UI/Preloader/Preloader";
import Backdrop from "../../components/UI/Backdrop/Backdrop";

const Articles = () => {
    const dispatch = useDispatch();
    const {news, loading} = useSelector(state => state.news);

    useEffect(() => {
        dispatch(fetchNews());
    }, [dispatch]);

    return (
        <>
            <Preloader show={loading}/>
            <Backdrop show={loading}/>
            <div className="container text-left">
            <div className="d-flex justify-content-between mb-3">
                <h1>News</h1>
                <NavLink to="/add/article"><Button className="align-self-center">Add new post</Button></NavLink>
            </div>
            {news.map(item => {
                return <ArticleItem
                    key={item.id}
                    image={item.image}
                    title={item.title}
                    date={item.date}
                    id={item.id}
                />
            })}
        </div>
        </>
    );
};

export default Articles;