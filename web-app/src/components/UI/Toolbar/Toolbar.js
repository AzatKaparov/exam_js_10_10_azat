import React from 'react';
import {Nav} from "react-bootstrap";
import {NavLink} from "react-router-dom";

const Toolbar = () => {
    return (
        <header className="container py-2">
            <Nav>
                <Nav.Item>
                    <NavLink to="/" className="mr-3">Home</NavLink>
                </Nav.Item>
                <Nav.Item>
                    <NavLink to="/add/article" className="mr-3">Add article</NavLink>
                </Nav.Item>
            </Nav>
        </header>
    );
};

export default Toolbar;