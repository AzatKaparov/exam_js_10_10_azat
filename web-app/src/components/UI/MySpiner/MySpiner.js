import React from 'react';
import {Spinner} from "react-bootstrap";

const MySpinner = ({show}) => {
    return show
        ? <Spinner animation="border" role="status"/>
        : null
};

export default MySpinner;