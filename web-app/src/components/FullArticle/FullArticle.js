import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArticle} from "../../store/actions/newsActions";
import {fetchComments} from "../../store/actions/commentsActions";
import Comment from "../Comment/Comment";
import CommentForm from "../CommentForm/CommentForm";
import Preloader from "../UI/Preloader/Preloader";
import Backdrop from "../UI/Backdrop/Backdrop";
import MySpinner from "../UI/MySpiner/MySpiner";
import './FullArticle.css';

const FullArticle = ({match}) => {
    const dispatch = useDispatch();
    const {article, loading} = useSelector(state => state.news);
    const {comments, commentLoading} = useSelector(state => state.comments);

    let commentItems;
    if (!comments || comments.length === 0) {
        commentItems = <p>There is no comments for that article</p>
    } else {
        commentItems = comments.map(comment => (
                <Comment
                    key={comment.id}
                    author={comment.author}
                    comment={comment.comment}
                    id={comment.id}
                />
            ));
    }

    useEffect(() => {
        dispatch(fetchArticle(match.params.id));
        dispatch(fetchComments(match.params.id));
    }, [match.params.id, dispatch]);

    return (
        <>
            <Preloader show={loading}/>
            <Backdrop show={loading}/>
            <div className="container text-left">
            <h1>{article.title}</h1>
            <p className="text-secondary">At: {article.date}</p>
            <div className="img-block d-flex justify-content-center">
                {article.image
                    ? <img className="article-img" src={`http://localhost:8000/uploads/${article.image}`} alt={article.image}/>
                    : null
                }
            </div>
            <p style={{fontSize: 24}}>{article.content}</p>
            <div className="comments-block">
                <CommentForm article_id={match.params.id} />
                <h2>Comments</h2>
                {commentLoading
                    ? <MySpinner show={commentLoading}/>
                    : commentItems
                }
            </div>
        </div>
        </>
    );
};

export default FullArticle;