import React, {useState} from 'react';
import {Button, Form} from "react-bootstrap";
import {useDispatch} from "react-redux";
import {createComment, fetchComments} from "../../store/actions/commentsActions";

const CommentForm = ({article_id}) => {
    const dispatch = useDispatch();
    const [options, setOptions] = useState({
        author: "",
        comment: "",
        news_id: article_id,
    });

    const onOptionsChange = e => {
        const name = e.target.name;
        const value = e.target.value;

        setOptions(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const onFormSubmit = async e => {
        e.preventDefault();
        await dispatch(createComment(options));
        await dispatch(fetchComments(article_id));
    }

    return (
        <>
            <h2 className="mt-4">Add comment</h2>
            <Form onSubmit={onFormSubmit} className="mb-4">
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                    <Form.Label>Name</Form.Label>
                    <Form.Control value={options.author} onChange={onOptionsChange} type="text" name="author" placeholder="Your name..." />
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Comment</Form.Label>
                    <Form.Control value={options.comment} onChange={onOptionsChange} as="textarea" rows={3} required={true} type="text" name="comment" placeholder="Comment..."/>
                </Form.Group>
                <Button variant="primary" type="submit">Submit</Button>
            </Form>
        </>
    );
};

export default CommentForm;