import React from 'react';
import './Comment.css';
import {Button} from "react-bootstrap";
import {useDispatch} from "react-redux";
import {deleteComment} from "../../store/actions/commentsActions";

const Comment = ({author, comment, id}) => {
    const dispatch = useDispatch();

    return (
        <div className="Comment px-2 py-3 mb-3">
            <h5>{author}</h5>
            <p>{comment}</p>
            <Button onClick={() => dispatch(deleteComment(id))} variant="danger">Delete</Button>
        </div>
    );
};

export default Comment;