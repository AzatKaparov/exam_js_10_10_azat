import React, {useState} from 'react';
import {Button, Form} from "react-bootstrap";
import {createArticle} from "../../store/actions/newsActions";
import {useDispatch, useSelector} from "react-redux";
import Preloader from "../UI/Preloader/Preloader";
import Backdrop from "../UI/Backdrop/Backdrop";

const ArticleForm = ({history}) => {
    const dispatch = useDispatch();
    const {loading} = useSelector(state => state.news);
    const [options, setOptions] = useState({
        title: "",
        content: "",
        image: "",
    });

    const onOptionsChange = e => {
        const name = e.target.name;
        const value = e.target.value;

        setOptions(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const onFileChange = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setOptions(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const onFormSubmit = async postData => {
        await dispatch(createArticle(postData));
        history.replace(`/`);
    }

    const submitFormHandler  = e => {
        e.preventDefault();
        const formData = new FormData();

        Object.keys(options).forEach(key => {
            formData.append(key, options[key]);
        })
        onFormSubmit(formData).catch(err => console.error(err));
    }

    return (
        <>
            <Preloader show={loading}/>
            <Backdrop show={loading}/>
            <div className="container text-left">
            <h1>Add new article</h1>
            <Form onSubmit={submitFormHandler}>
                <Form.Group className="mb-3">
                    <Form.Label>Title</Form.Label>
                    <Form.Control onChange={onOptionsChange} value={options.title} name="title" type="text" placeholder="Your title..." />
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Content</Form.Label>
                    <Form.Control onChange={onOptionsChange} value={options.content} name="content" as="textarea" rows={5} />
                </Form.Group>
                <Form.Group controlId="formFile" className="mb-3">
                    <Form.Label>Image</Form.Label>
                    <Form.Control onChange={onFileChange} name="image" type="file" />
                </Form.Group>
                <Button type="submit">Submit</Button>
            </Form>
        </div>
        </>
    );
};

export default ArticleForm;