import React from 'react';
import {Button, Card} from "react-bootstrap";
import {useDispatch} from "react-redux";
import {deleteArticle} from "../../store/actions/newsActions";
import {NavLink} from "react-router-dom";

const ArticleItem = ({image, date, title, id}) => {
    const dispatch = useDispatch();

    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Img style={{maxHeight: 200, width: 'auto', maxWidth: 1000}} variant="top" src={image ? `http://localhost:8000/uploads/${image}` : null} />
                <Card.Title className="my-2">{title}</Card.Title>
                <div className="d-flex align-items-center">
                    <p className="mx-2 my-0">{date}</p>
                    <NavLink to={`article/${id}`} className="mx-2 btn btn-primary">Read full</NavLink>
                    <Button onClick={() => dispatch(deleteArticle(id))} className="mx-2" variant="danger">Delete</Button>
                </div>
            </Card.Body>
        </Card>
    );
};

export default ArticleItem;