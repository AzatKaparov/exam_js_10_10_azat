import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Articles from "./containers/Articles/Articles";
import ArticleForm from "./components/ArticleForm/ArticleForm";
import FullArticle from "./components/FullArticle/FullArticle";

function App() {
  return (
      <div className="App">
        <BrowserRouter>
          <Layout>
            <Switch>
              <Route path="/" exact component={Articles}/>
              <Route path="/add/article" exact component={ArticleForm}/>
              <Route path="/article/:id" exact component={FullArticle}/>
              <Route render={() => <h1>Not found</h1>}/>
            </Switch>
          </Layout>
        </BrowserRouter>
      </div>
  );
}

export default App;
