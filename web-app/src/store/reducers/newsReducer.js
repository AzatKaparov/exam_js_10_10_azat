import {
    CREATE_NEWS_FAILURE,
    CREATE_NEWS_REQUEST,
    CREATE_NEWS_SUCCESS,
    DELETE_NEWS_FAILURE,
    DELETE_NEWS_REQUEST,
    DELETE_NEWS_SUCCESS, FETCH_ARTICLE_FAILURE, FETCH_ARTICLE_REQUEST, FETCH_ARTICLE_SUCCESS,
    FETCH_NEWS_FAILURE,
    FETCH_NEWS_REQUEST,
    FETCH_NEWS_SUCCESS
} from "../actions/newsActions";

const initialState = {
    news: [],
    loading: false,
    error: null,
    article: {},
}

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_REQUEST:
            return {...state, loading: true};
        case FETCH_NEWS_SUCCESS:
            return {...state, news: action.payload, loading: false};
        case FETCH_NEWS_FAILURE:
            return {...state, error: action.payload};
        case CREATE_NEWS_REQUEST:
            return {...state, loading: true};
        case CREATE_NEWS_SUCCESS:
            return {...state, loading: false, news: [...state.news, action.payload]};
        case CREATE_NEWS_FAILURE:
            return {...state, loading: false, error: action.payload};
        case DELETE_NEWS_REQUEST:
            return {...state, loading: true};
        case DELETE_NEWS_SUCCESS:
            const filtered = state.news.filter(item => item.id !== action.payload);
            return {...state, loading: false, news: filtered};
        case DELETE_NEWS_FAILURE:
            return {...state, error: action.payload};
        case FETCH_ARTICLE_REQUEST:
            return {...state, loading: true};
        case FETCH_ARTICLE_SUCCESS:
            return {...state, loading: false, article: action.payload};
        case FETCH_ARTICLE_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
};

export default newsReducer;