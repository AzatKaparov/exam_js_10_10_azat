import {
    CREATE_COMMENT_FAILURE,
    CREATE_COMMENT_REQUEST, CREATE_COMMENT_SUCCESS,
    DELETE_COMMENT_FAILURE,
    DELETE_COMMENT_REQUEST,
    DELETE_COMMENT_SUCCESS,
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS
} from "../actions/commentsActions";

const initialState = {
    comments: [],
    commentLoading: false,
    error: null,
}

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state, commentLoading: true};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.payload, commentLoading: false};
        case FETCH_COMMENTS_FAILURE:
            return {...state, error: action.payload, comments: [], commentLoading: false};
        case CREATE_COMMENT_REQUEST:
            return {...state, commentLoading: true};
        case CREATE_COMMENT_SUCCESS:
            return {...state, commentLoading: false, comments: [...state.comments, action.payload]};
        case CREATE_COMMENT_FAILURE:
            return {...state, commentLoading: false, error: action.payload};
        case DELETE_COMMENT_REQUEST:
            return {...state, commentLoading: true};
        case DELETE_COMMENT_SUCCESS:
            const filtered = state.comments.filter(item => item.id !== action.payload);
            return {...state, commentLoading: false, comments: filtered};
        case DELETE_COMMENT_FAILURE:
            return {...state, commentLoading: false, error: action.payload};
        default:
            return state;
    }
};

export default commentsReducer;