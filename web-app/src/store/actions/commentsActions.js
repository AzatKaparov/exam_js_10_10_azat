import axiosApi from "../../axiosApi";
import {CREATE_NEWS_FAILURE, CREATE_NEWS_REQUEST, CREATE_NEWS_SUCCESS} from "./newsActions";

export const FETCH_COMMENTS_REQUEST = "FETCH_COMMENTS_REQUEST";
export const FETCH_COMMENTS_SUCCESS = "FETCH_COMMENTS_SUCCESS";
export const FETCH_COMMENTS_FAILURE = "FETCH_COMMENTS_FAILURE";

export const CREATE_COMMENT_REQUEST = 'CREATE_COMMENT_REQUEST';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const CREATE_COMMENT_FAILURE = 'CREATE_COMMENT_FAILURE';

export const DELETE_COMMENT_REQUEST = 'DELETE_COMMENT_REQUEST';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';
export const DELETE_COMMENT_FAILURE = 'DELETE_COMMENT_FAILURE';

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, payload: comments});
export const fetchCommentsFailure = err => ({type: FETCH_COMMENTS_FAILURE, payload: err});

export const createCommentRequest = () => ({type: CREATE_NEWS_REQUEST});
export const createCommentSuccess = comment => ({type: CREATE_NEWS_SUCCESS, payload: comment});
export const createCommentFailure = err => ({type: CREATE_NEWS_FAILURE, payload: err});

export const deleteCommentRequest = () => ({type: DELETE_COMMENT_REQUEST});
export const deleteCommentSuccess = id => ({type: DELETE_COMMENT_SUCCESS, payload: id});
export const deleteCommentFailure = err => ({type: DELETE_COMMENT_FAILURE, payload: err});

export const fetchComments = id => {
    return async dispatch => {
        dispatch(fetchCommentsRequest());

        try {
            const response = await axiosApi.get(`/comments?news_id=${id}`);
            dispatch(fetchCommentsSuccess(response.data));
        } catch (e) {
            dispatch(fetchCommentsFailure(e));
        }
    };
};

export const createComment = data => {
    return async dispatch => {
        dispatch(createCommentRequest());

        try {
            const response = await axiosApi.post(`/comments`, data);
            dispatch(createCommentSuccess(response.data));
        } catch (e) {
            dispatch(createCommentFailure(e));
            throw e;
        }
    };
};

export const deleteComment = id => {
    return async dispatch => {
        dispatch(deleteCommentRequest());

        try {
            await axiosApi.delete(`/comments/${id}`);
            dispatch(deleteCommentSuccess(id));
        } catch (e) {
            dispatch(deleteCommentFailure(e));
            throw e;
        }
    }
}