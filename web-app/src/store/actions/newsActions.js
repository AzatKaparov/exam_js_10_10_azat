import axiosApi from "../../axiosApi";

export const FETCH_NEWS_REQUEST = "FETCH_NEWS_REQUEST";
export const FETCH_NEWS_SUCCESS = "FETCH_NEWS_SUCCESS";
export const FETCH_NEWS_FAILURE = "FETCH_NEWS_FAILURE";

export const FETCH_ARTICLE_REQUEST = "FETCH_ARTICLE_REQUEST";
export const FETCH_ARTICLE_SUCCESS = "FETCH_ARTICLE_SUCCESS";
export const FETCH_ARTICLE_FAILURE = "FETCH_ARTICLE_FAILURE";

export const CREATE_NEWS_REQUEST = 'CREATE_NEWS_REQUEST';
export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';
export const CREATE_NEWS_FAILURE = 'CREATE_NEWS_FAILURE';

export const DELETE_NEWS_REQUEST = 'DELETE_NEWS_REQUEST';
export const DELETE_NEWS_SUCCESS = 'DELETE_NEWS_SUCCESS';
export const DELETE_NEWS_FAILURE = 'DELETE_NEWS_FAILURE';

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, payload: news});
export const fetchNewsFailure = err => ({type: FETCH_NEWS_FAILURE, payload: err});

export const fetchArticleRequest = () => ({type: FETCH_ARTICLE_REQUEST});
export const fetchArticleSuccess = article => ({type: FETCH_ARTICLE_SUCCESS, payload: article});
export const fetchArticleFailure = err => ({type: FETCH_ARTICLE_FAILURE, payload: err});

export const createNewsRequest = () => ({type: CREATE_NEWS_REQUEST});
export const createNewsSuccess = article => ({type: CREATE_NEWS_SUCCESS, payload: article});
export const createNewsFailure = err => ({type: CREATE_NEWS_FAILURE, payload: err});

export const deleteNewsRequest = () => ({type: DELETE_NEWS_REQUEST});
export const deleteNewsSuccess = id => ({type: DELETE_NEWS_SUCCESS, payload: id});
export const deleteNewsFailure = err => ({type: DELETE_NEWS_FAILURE, payload: err});

export const fetchNews = () => {
    return async dispatch => {
        dispatch(fetchNewsRequest());

        try {
            const response = await axiosApi.get(`/news`);
            dispatch(fetchNewsSuccess(response.data));
        } catch (e) {
            dispatch(fetchNewsFailure(e));
        }
    };
};

export const fetchArticle = id => {
    return async dispatch => {
        dispatch(fetchArticleRequest());

        try {
            const response = await axiosApi.get(`/news/${id}`);
            dispatch(fetchArticleSuccess(response.data));
        } catch (e) {
            dispatch(fetchArticleFailure(e));
        }
    }
}

export const createArticle = articleData => {
    return async dispatch => {
        dispatch(createNewsRequest());

        try {
            const response = await axiosApi.post(`/news`, articleData);
            dispatch(createNewsSuccess(response.data));
        } catch (e) {
            dispatch(createNewsFailure(e));
            throw e;
        }
    };
};

export const deleteArticle = id => {
    return async dispatch => {
        dispatch(deleteNewsRequest());

        try {
            await axiosApi.delete(`/news/${id}`);
            dispatch(deleteNewsSuccess(id));
        } catch (e) {
            dispatch(deleteNewsFailure(e));
            throw e;
        }
    }
}