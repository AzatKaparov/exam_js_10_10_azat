DROP DATABASE if exists exam_9;
CREATE DATABASE if not exists exam_9;

use exam_9;

CREATE TABLE if not exists news (
    id int not null auto_increment primary key,
    title varchar(255) not null,
    content longtext not null,
    image varchar(255) null,
    date datetime default current_timestamp
);

CREATE TABLE if not exists comments (
    id int not null auto_increment primary key,
    news_id int not null,
    author varchar(255) null default "Anonymous",
    comment text not null,
    constraint comment_news_id_fk
    foreign key (news_id)
    references news(id)
    on delete CASCADE
    on update CASCADE
);

INSERT INTO news (title, content)
values ('First one', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry'),
       ('Second one', 'Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s'),
       ('TMNT', '«Черепашки-ниндзя» — американский компьютерный анимационный мультсериал. Премьера в США
состоялась 29 сентября 2012 года на канале Nickelodeon. Это третий анимационный проект, спродюсированный
компанией Nickelodeon Animation Studio, о Черепашках Мутантах Ниндзя.');

INSERT INTO comments (news_id, author, comment)
values (1, "Azat", "Cool!"),
       (1, "someone", "Nice"),
       (2, "idc", "Life is go on"),
       (2, "Frodo", "Where is my ring?"),
       (3, "kid", "Damn, i like them!");