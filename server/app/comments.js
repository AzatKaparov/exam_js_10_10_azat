const express = require("express");
const mysql = require('../SQLconfig');
const router = express.Router();


router.get("/", async (req, res) => {
    if (req.query.news_id) {
        const [filtered] = await mysql.getConnection().query(
            "SELECT * FROM ?? where news_id = ?",
            ['comments', req.query.news_id]
        );
        if (!filtered || filtered.length === 0) {
            return res.status(404).send({error: "There is no comments for this article"})
        }
        res.send(filtered);
    } else {
        const [comments] = await mysql.getConnection().query(
            `SELECT * FROM ??`,
            ['comments']);
        res.send(comments);
    }

});

router.post("/", async (req, res) => {
    if (!req.body.news_id || req.body.news_id === "" || req.body.news_id === null) {
        return res.status(400).send({error: 'Data not valid'});
    }
    if (!req.body.comment || req.body.comment === "" || req.body.comment === null) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const [newsWithComments] = await mysql.getConnection().query(
        'SELECT * FROM ?? where id = ?',
        ['news', req.body.news_id]
    );

    if (!newsWithComments || newsWithComments.length === 0) {
        return res.status(404).send({error: `There is no articles with ID=${req.body.news_id}`});
    }

    const comment = {
        news_id: req.body.news_id,
        comment: req.body.comment,
        author: req.body.author ? req.body.author : "Anonymous",
    };

    const newComment = await mysql.getConnection().query(
        'INSERT INTO ?? (news_id, comment, author) values (?, ?, ?)',
        ['comments', comment.news_id, comment.comment, comment.author]
    );

    const [exact] = await mysql.getConnection().query(
        'SELECT * FROM ?? where id = ?',
        ['comments', newComment[0].insertId]
    );

    res.send(exact[0]);
});

router.delete("/:id", async (req, res) => {
    if (!req.params.id) {
        return res.status(400).send({error: "Invalid request"});
    }

    await mysql.getConnection().query(
        "DELETE FROM ?? where id = ?",
        ['comments', req.params.id]
    ).catch(e => console.error(e));

    res.send({
        message: "Comment deleted"
    });
});

module.exports = router;