const express = require("express");
const mysql = require('../SQLconfig');
const multer = require('multer');
const config = require('../config');
const {nanoid} = require('nanoid');
const path = require('path');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


router.get("/", async (req, res) => {
    const [news] = await mysql.getConnection().query(
        `SELECT id, title, image, date FROM ??`,
        ['news']);
    res.send(news);
});

router.get('/:id', async (req, res) => {
    const [article] = await mysql.getConnection().query(
        `SELECT * FROM ?? where id = ?`,
        ['news', req.params.id]);
    if (!article || article.length === 0) {
        return res.status(404).send({error: 'Article not found'});
    }
    res.send(article[0]);
});


router.post("/", upload.single('image'), async (req, res) => {
    if (!req.body.title || req.body.title === "" || req.body.title === null) {
        return res.status(400).send({error: 'Data not valid'});
    }
    if (!req.body.content || req.body.content === "" || req.body.content === null) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const article = {
        title: req.body.title,
        content: req.body.content,
    };

    if (req.file) {
        article.image = req.file.filename;
    }

    const newArticle = await mysql.getConnection().query(
        'INSERT INTO ?? (title, content, image) values (?, ?, ?)',
        ['news', article.title, article.content, article.image]
    );

    const [exact] = await mysql.getConnection().query(
        'SELECT * FROM ?? where id = ?',
        ['news', newArticle[0].insertId]
    );

    res.send(exact[0]);
});

router.delete("/:id", async (req, res) => {
    if (!req.params.id) {
        return res.status(400).send({error: "Invalid request"});
    }

    await mysql.getConnection().query(
        "DELETE FROM ?? where id = ?",
        ['news', req.params.id]
    ).catch(e => console.error(e));

    res.send({
        message: "Article deleted"
    });
});

module.exports = router;