const express = require('express');
const cors = require('cors');
const app = express();
const news = require('./app/news');
const comments = require('./app/comments');
const port = 8000;
const mysql = require('./SQLconfig');


app.use(express.json());
app.use(cors());
app.use('/news', news);
app.use('/comments', comments);
app.use(express.static('public'));


mysql.connect().catch(e => console.error(e));

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});